from . import models
from . import serializers
from rest_framework import viewsets, permissions


class Search_QueryViewSet(viewsets.ModelViewSet):
    """ViewSet for the Search_Query class"""

    queryset = models.Search_Query.objects.all()
    serializer_class = serializers.Search_QuerySerializer
    permission_classes = [permissions.IsAuthenticated]


class RestaurantViewSet(viewsets.ModelViewSet):
    """ViewSet for the Restaurant class"""

    queryset = models.Restaurant.objects.all()
    serializer_class = serializers.RestaurantSerializer
    permission_classes = [permissions.IsAuthenticated]


