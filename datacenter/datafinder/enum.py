from enumfields import Enum

class ApiProviderType(Enum):
    YELP = 'YE'
    YELLOW_PAGES = 'YL'
    GOOGLE = 'GO'