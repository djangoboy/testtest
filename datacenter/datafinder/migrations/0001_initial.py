# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-02-10 21:02
from __future__ import unicode_literals

from django.db import migrations, models
import enumfields.fields
import datafinder.enum


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='APiProvider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('provider', models.CharField(max_length=255)),
                ('key', models.CharField(max_length=255)),
                ('secret', models.CharField(blank=True, max_length=255)),
                ('daily_limit', models.PositiveIntegerField(editable=False, null=True)),
            ],
            options={
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('unique_id', models.CharField(max_length=255, unique=True)),
                ('phone', models.CharField(max_length=200, null=True)),
                ('location', models.CharField(max_length=1000)),
                ('categories', models.CharField(max_length=1000, null=True)),
                ('website', models.URLField(max_length=255, null=True)),
                ('lat', models.DecimalField(decimal_places=15, max_digits=50)),
                ('lng', models.DecimalField(decimal_places=15, max_digits=50)),
                ('cuisines', models.CharField(max_length=1000, null=True)),
                ('yellowPagesData', models.CharField(max_length=1000, null=True)),
                ('yelpData', models.CharField(max_length=1000, null=True)),
                ('googleDataInfo', models.CharField(max_length=1000, null=True)),
                ('zohoCrmId', models.CharField(max_length=1000, null=True)),
                ('lastUpdated', models.DateField(auto_now=True)),
            ],
            options={
                'ordering': ('-pk',),
            },
        ),
        migrations.CreateModel(
            name='Search_Query',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('search', models.CharField(max_length=255)),
                ('location', models.CharField(max_length=1000)),
                ('completed', models.BooleanField(default=False)),
                ('failed', models.BooleanField(default=False)),
                ('created_on', models.DateField(auto_now_add=True)),
                ('api_provider', enumfields.fields.EnumField(enum=datafinder.enum.ApiProviderType, max_length=10)),
            ],
            options={
                'ordering': ('-pk',),
            },
        ),
    ]
