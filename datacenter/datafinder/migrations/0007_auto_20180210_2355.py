# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2018-02-10 23:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datafinder', '0006_auto_20180210_2322'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apiprovider',
            name='daily_limit',
            field=models.PositiveIntegerField(default=995, editable=False),
        ),
    ]
