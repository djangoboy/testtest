from django import forms
# from .models import Search_Query, Restaurant, APiProvider
from datafinder.models import Restaurant, Search_Query, APiProvider


class Search_QueryForm(forms.ModelForm):
    class Meta:
        model = Search_Query
        fields = ['search', 'location', 'radius', 'api_provider']


class RestaurantForm(forms.ModelForm):
    class Meta:
        model = Restaurant
        fields = ['name', 'phone','lat','lng','location', 'categories', 'website', 'cuisines', 'yellowPagesData',
                  'yelpData', 'googleDataInfo']


class APiProviderForm(forms.ModelForm):
    class Meta:
        model = APiProvider
        fields = ['provider', 'key', 'secret']
