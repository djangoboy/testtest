from django.conf.urls import url, include
from django.contrib.auth.decorators import login_required
from datafinder import views


# from . import views

urlpatterns = (
    # urls for datafinder app
    url(r'^searchquery-list/$', login_required(views.Search_QueryListView.as_view()), name='search_query_list'),
    url(r'^searchquery-list/create/$', login_required(views.Search_QueryCreateView.as_view()), name='search_querycreateview'),
    url(r'^searchquery-list/update/(?P<pk>\S+)/$', login_required(views.Search_QueryUpdateView.as_view()), name='search_queryupdateview'),

    url(r'^resturant-list/$', login_required(views.RestaurantListView.as_view()), name='restaurantlistView'),
    url(r'^resturant-list/create$', login_required(views.RestaurantCreateView.as_view()), name='restaurantcreateview'),
    url(r'^resturant-list/update/(?P<pk>\S+)/', login_required(views.RestaurantUpdateView.as_view()), name='restaurantupdateview'),

    url(r'^apiprovider/$', login_required(views.APiProviderListView.as_view()), name='restfinder_apiprovider_list'),
    url(r'^apiprovider/create/$', login_required(views.APiProviderCreateView.as_view()), name='restfinder_apiprovider_create'),
    url(r'^apiprovider/detail/(?P<pk>\S+)/', login_required(views.APiProviderDetailView.as_view()), name='restfinder_apiprovider_detail'),
    url(r'^apiprovider/update/(?P<pk>\S+)/$', login_required(views.APiProviderUpdateView.as_view()), name='restfinder_apiprovider_update'),
    )

