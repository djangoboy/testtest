from django.contrib import admin
from django import forms
from .models import Search_Query, Restaurant, APiProvider

class Search_QueryAdminForm(forms.ModelForm):

    class Meta:
        model = Search_Query
        fields = '__all__'


class Search_QueryAdmin(admin.ModelAdmin):
    form = Search_QueryAdminForm
    list_display = ['search', 'location', 'completed', 'failed', 'created_on', 'api_provider']
    readonly_fields = ['failed', 'created_on']

admin.site.register(Search_Query, Search_QueryAdmin)


class RestaurantAdminForm(forms.ModelForm):

    class Meta:
        model = Restaurant
        fields = '__all__'

class RestaurantAdmin(admin.ModelAdmin):
    form = RestaurantAdminForm
    list_display = ['name', 'phone', 'lat', 'lng','location', 'categories', 'website', 'cuisines', 'yellowPagesData', 'yelpData', 'googleDataInfo', 'lastUpdated']
    #readonly_fields = ['name', 'address', 'phone', 'location', 'categories', 'website', 'cuisines', 'yellowPagesData', 'yelpData', 'googleDataInfo', 'lastUpdated']

admin.site.register(Restaurant, RestaurantAdmin)


class APiProviderAdminForm(forms.ModelForm):

    class Meta:
        model = APiProvider
        fields = '__all__'


class APiProviderAdmin(admin.ModelAdmin):
    form = APiProviderAdminForm
    list_display = ['provider', 'key', 'secret', 'daily_limit']
    readonly_fields = ['provider', 'key', 'secret', 'daily_limit']

admin.site.register(APiProvider, APiProviderAdmin)