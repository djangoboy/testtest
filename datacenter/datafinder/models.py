from django.db import models as models
from enumfields import EnumField
from django.shortcuts import reverse
from datafinder.enum import ApiProviderType

class Search_Query(models.Model):
    search = models.CharField(max_length=255)
    location = models.CharField(max_length=1000)
    radius = models.FloatField(max_length=1000)
    completed = models.BooleanField(blank=True, default=False) #what if this is null=True
    failed = models.BooleanField(blank=False, default=False)#what if this is null=True
    created_on = models.DateField(auto_now_add=True, blank=False)
    api_provider = EnumField(ApiProviderType)

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return self.search

    def get_absolute_url(self):
        return reverse('datafinder:search_query_list')

    def get_update_url(self):
        return reverse('datafinder:search_queryupdateview', args=(self.pk,))


class Restaurant(models.Model):
    name = models.CharField(max_length=255)
    unique_id = models.CharField(max_length=255, unique=True)
    phone = models.CharField(max_length=200, null=True)
    location = models.CharField(max_length=1000)
    categories = models.CharField(max_length=1000, null=True)
    website = models.URLField(max_length=255, null=True)
    lat = models.DecimalField(decimal_places=15, max_digits=50, null=True)
    lng = models.DecimalField(decimal_places=15, max_digits=50, null=True)
    cuisines = models.CharField(max_length=1000, null=True)
    yellowPagesData = models.CharField(max_length=1000, null=True)
    yelpData = models.CharField(max_length=1000, null=True)
    googleDataInfo = models.CharField(max_length=1000, null=True)
    zohoCrmId = models.CharField(max_length=1000, null=True)
    lastUpdated = models.DateField(auto_now=True)

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return self.name


    def get_absolute_url(self):
        return reverse('datafinder:restaurantlistView')

class APiProvider(models.Model):

    # Fields
    provider = EnumField(ApiProviderType)
    key = models.CharField(max_length=255)
    secret = models.CharField(max_length=255, blank=True)
    daily_limit = models.PositiveIntegerField(editable=False, default=995)

    class Meta:
        ordering = ('-pk',)


    def get_absolute_url(self):
        return reverse('datafinder:restfinder_apiprovider_detail', args=(self.pk,))


    # def get_update_url(self):
    #     return reverse('restfinder_apiprovider_update', args=(self.pk,))




