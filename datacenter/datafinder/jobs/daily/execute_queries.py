from googleplaces import GooglePlaces, types, lang
from django_extensions.management.jobs import DailyJob
from datafinder.models import Restaurant, Search_Query, APiProvider

class Job(DailyJob):
    def execute(self):
        all_api_providers = APiProvider.objects.all()
        for provider in all_api_providers:
            google_places = GooglePlaces(provider.key)
            all_searches = Search_Query.objects.filter(completed=False)
            for search in set(all_searches):#set(all_searches):
                location, keyword, radius = search.location, search.search, search.radius
                query_result = google_places.nearby_search(location=location, keyword=keyword, radius=radius)
                for place in query_result.places:
                    resturant,_ = Restaurant.objects.get_or_create(unique_id=place.place_id)
                    place.get_details()
                    resturant.name = place.name
                    resturant.phone = str(place.local_phone_number) + str(place.international_phone_number)
                    resturant.website = place.website
                    resturant.lat = place.geo_location['lat']
                    resturant.lng = place.geo_location['lng']
                    resturant.location = place.formatted_address
                    resturant.categories = ''.join(place.types)
                    resturant.googleDataInfo = place.rating
                    resturant.save()
                    while query_result.has_next_page_token:
                        query_result_next_page = google_places.nearby_search(pagetoken=query_result.next_page_token)
                        print(query_result.has_next_page_token)
                        query_result = query_result_next_page
                        for place in query_result.places:
                            resturant, _ = Restaurant.objects.get_or_create(unique_id=place.place_id)
                            place.get_details()
                            resturant.name = place.name
                            resturant.phone = str(place.local_phone_number) + str(place.international_phone_number)
                            resturant.website = place.website
                            resturant.lat = place.geo_location['lat']
                            resturant.lng = place.geo_location['lng']
                            resturant.location = place.formatted_address
                            resturant.categories = ''.join(place.types)
                            resturant.googleDataInfo = place.rating
                            resturant.save()
                    else:
                        print(query_result.has_next_page_token)
                        break
                search.completed = True
                search.save()






# from googleplaces import GooglePlaces, types, lang
# from django_extensions.management.jobs import DailyJob
# from datafinder.models import Restaurant, Search_Query, APiProvider
#
# class Job(DailyJob):
#     def execute(self):
#         all_api_providers = APiProvider.objects.all()
#         for provider in all_api_providers:
#             google_places = GooglePlaces(provider.key)
#             all_searches = Search_Query.objects.filter(completed=False)
#             for search in set(all_searches):#set(all_searches):
#                 location, keyword, radius = search.location, search.search, search.radius
#                 query_result = google_places.nearby_search(location=location, keyword=keyword, radius=radius)
#                 print(query_result)
#                 print(query_result.has_next_page_token)
#                 for place in query_result.places:
#                     print(query_result)
#                     print(query_result.has_next_page_token)
#                     resturant,_ = Restaurant.objects.get_or_create(unique_id=place.place_id)
#                     place.get_details()
#                     print(place.name)
#                     resturant.name = place.name
#                     resturant.phone = str(place.local_phone_number) + str(place.international_phone_number)
#                     resturant.website = place.website
#                     resturant.lat = place.geo_location['lat']
#                     resturant.lng = place.geo_location['lng']
#                     resturant.location = place.formatted_address
#                     resturant.categories = ''.join(place.types)
#                     resturant.googleDataInfo = place.rating
#                     resturant.save()
#                     print(query_result)
#                     if query_result.has_next_page_token:
#                         query_result = google_places.nearby_search(pagetoken=query_result.next_page_token)
#                         print(query_result)
#                         print(query_result.has_next_page_token)
#                         continue
#                 search.completed = True
#                 search.save()
#                 #
#                 #     print(query_result_next_page)
#         #
#         # if query_result.has_next_page_token:
#         #     query_result_next_page = google_places.nearby_search(pagetoken=query_result.next_page_token)
#         #     query_result = query_result_next_page
#         # else:
#         #     query_result = query_result
#
#
#
#





#another working code


#                 # if query_result.has_next_page_token:
#                 #     query_result_next_page = google_places.nearby_search(pagetoken=query_result.next_page_token)
#                 #     print(query_result_next_page)
#         #
#         # if query_result.has_next_page_token:
#         #     query_result_next_page = google_places.nearby_search(pagetoken=query_result.next_page_token)
#         #     query_result = query_result_next_page
#         # else:
#         #     query_result = query_result



# def save_obj_2_db(unique_id):
#     resturant, _ = Restaurant.objects.get_or_create(unique_id=unique_id)#(unique_id=place.place_id)
#     place.get_details()
#     resturant.name = place.name
#     resturant.phone = str(place.local_phone_number) + str(place.international_phone_number)
#     resturant.website = place.website
#     resturant.lat = place.geo_location['lat']
#     resturant.lng = place.geo_location['lng']
#     resturant.location = place.formatted_address
#     resturant.categories = ''.join(place.types)
#     resturant.googleDataInfo = place.rating
#     resturant.save()
#
#
# search.completed = True
# search.save()
#
#
#
#
#
#
#
#
#
#







#Beautifully working code -->trying to parse anyother page too
# from googleplaces import GooglePlaces, types, lang
# from django_extensions.management.jobs import DailyJob
# from datafinder.models import Restaurant, Search_Query, APiProvider
#
# class Job(DailyJob):
#     def execute(self):
#         all_api_providers = APiProvider.objects.all()
#         for provider in all_api_providers:
#             google_places = GooglePlaces(provider.key)
#             all_searches = Search_Query.objects.filter(completed=False)
#         # print(APiProvider.daily_limit)
#         # for i in range(995):
#             for search in set(all_searches):#set(all_searches):
#                 location, keyword, radius = search.location, search.search, search.radius
#                 query_result = google_places.nearby_search(location=location, keyword=keyword, radius=radius)
#                 for place in query_result.places:
#                     resturant,_ = Restaurant.objects.get_or_create(unique_id=place.place_id)
#                     place.get_details()
#                     resturant.name = place.name
#                     resturant.phone = str(place.local_phone_number) + str(place.international_phone_number)
#                     resturant.website = place.website
#                     resturant.lat = place.geo_location['lat']
#                     resturant.lng = place.geo_location['lng']
#                     resturant.location = place.formatted_address
#                     resturant.categories = ''.join(place.types)
#                     resturant.googleDataInfo = place.rating
#                     resturant.save()
#                 search.completed = True
#                 search.save()
#                 # if query_result.has_next_page_token:
#                 #     query_result_next_page = google_places.nearby_search(pagetoken=query_result.next_page_token)
#                 #     print(query_result_next_page)
#         #
#         # if query_result.has_next_page_token:
#         #     query_result_next_page = google_places.nearby_search(pagetoken=query_result.next_page_token)
#         #     query_result = query_result_next_page
#         # else:
#         #     query_result = query_result
#
#
#
# # def save_obj_2_db(unique_id, location, keyword, radius):
# #     google_places = GooglePlaces(provider.key)
# #     query_result = google_places.nearby_search(location=location, keyword=keyword, radius=radius)
# #     for place in query_result.places:
# #         resturant, _ = Restaurant.objects.get_or_create(unique_id=place.place_id)
# #         place.get_details()
# #         resturant.name = place.name
# #         resturant.phone = str(place.local_phone_number) + str(place.international_phone_number)
# #         resturant.website = place.website
# #         resturant.lat = place.geo_location['lat']
# #         resturant.lng = place.geo_location['lng']
# #         resturant.location = place.formatted_address
# #         resturant.categories = ''.join(place.types)
# #         resturant.googleDataInfo = place.rating
# #         resturant.save()
# #     search.completed = True
# #     search.save()
#
#
