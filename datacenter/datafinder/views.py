from django.views.generic import DetailView, ListView, UpdateView, CreateView

from datafinder.forms import RestaurantForm, Search_QueryForm, APiProviderForm
from datafinder.models import Restaurant, Search_Query, APiProvider

# from .models import Search_Query, Restaurant
# from .forms import Search_QueryForm, RestaurantForm


class Search_QueryListView(ListView):
    model = Search_Query
    # x = list(Search_Query.objects.all().filter(completed=False).values_list('location', flat=True))
    # print(x)

class Search_QueryCreateView(CreateView):
    model = Search_Query
    form_class = Search_QueryForm


class Search_QueryDetailView(DetailView):
    model = Search_Query


class Search_QueryUpdateView(UpdateView):
    model = Search_Query
    form_class = Search_QueryForm


class RestaurantListView(ListView):
    model = Restaurant


class RestaurantCreateView(CreateView):
    model = Restaurant
    form_class = RestaurantForm


class RestaurantDetailView(DetailView):
    model = Restaurant


class RestaurantUpdateView(UpdateView):
    model = Restaurant
    form_class = RestaurantForm

class APiProviderListView(ListView):
    model = APiProvider

class APiProviderCreateView(CreateView):
    model = APiProvider
    form_class = APiProviderForm

class APiProviderDetailView(DetailView):
    model = APiProvider

class APiProviderUpdateView(UpdateView):
    model = APiProvider
    form_class = APiProviderForm

        # Resturaunt.objects.all().filter(completed='').value_lists('search_query')